#ifndef BaseFindDialog_h
#define BaseFindDialog_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "BaseDialog.h"
#include "BaseFindWidget.h"
#include "Counter.h"

//* find dialog for text editor widgets
class BaseFindDialog: public BaseDialog, private Base::Counter<BaseFindDialog>
{

    //* Qt meta object declaration
    Q_OBJECT

    public:

    //* constructor
    explicit BaseFindDialog( QWidget* parent = nullptr, Qt::WindowFlags WindowFlags = 0 );

    //*@name accessors
    //@{

    //* string to find
    QString text() const
    { return baseFindWidget_->text(); }

    //* get selection
    virtual TextSelection selection( bool value ) const
    { return baseFindWidget_->selection( value ); }

    //* find widget
    BaseFindWidget& baseFindWidget() const
    { return *baseFindWidget_; }

    //* retrieve editor
    CustomComboBox& editor() const
    { return baseFindWidget_->editor(); }

    //@}

    //*@name modifiers
    //@{

    //* string to find
    void setText( const QString& text )
    { baseFindWidget_->setText( text ); }

    //* synchronize searched strings and ComboBox
    virtual void synchronize()
    { baseFindWidget_->synchronize(); }

    //* enable/disable entire word
    virtual void enableEntireWord( bool value )
    { baseFindWidget_->enableEntireWord( value ); }

    //* enable/disable RegExp
    virtual void enableRegExp( bool value )
    { baseFindWidget_->enableRegExp( value ); }

    //* set base find widget
    void setBaseFindWidget( BaseFindWidget* );

    //@}

    Q_SIGNALS:

    //* emitted when Find is pressed
    void find( TextSelection );

    public Q_SLOTS:

    //* take action when at least one match is found
    void matchFound()
    { baseFindWidget_->matchFound(); }

    //* take action when no match is found
    void noMatchFound()
    { baseFindWidget_->noMatchFound(); }

    private:

    //* find widget
    BaseFindWidget* baseFindWidget_ = nullptr;

};
#endif
