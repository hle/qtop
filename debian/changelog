qtop (2.3.4-2) unstable; urgency=medium

  * debian/control:
    - Bump Standards-Version to 4.1.5.
    - Depend on debhelper >= 11 instead of >= 10.
    - Update Vcs-* fields for Salsa migration.
  * Bump compat from 10 to 11.
  * debian/copyright:
    - Update copyright years.
  * Bump watch version to 4.
  * Remove trailing whitespaces from d/changelog.

 -- Hugo Lefeuvre <hle@debian.org>  Tue, 31 Jul 2018 16:52:12 +0800

qtop (2.3.4-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 4.1.0.
    - Depend on debhelper >= 10 instead of >= 9.
    - Depend on libqt5-dev instead of libqt4-dev (Closes: #875162).
    - Update Homepage field to use http (https one is dead)
    - Add missing dependencies on libqt5x11extras5-dev and
      qtbase5-private-dev.
  * Bump compat from 9 to 10.
  * debian/copyright:
    - Update copyright years.
  * Update manpage for new upstream release.
  * Refresh patches or new upstream release.

 -- Hugo Lefeuvre <hle@debian.org>  Thu, 05 Oct 2017 13:13:36 +0200

qtop (2.3.3-1) unstable; urgency=medium

  * New upstream release.
  * Remove debian/menu (useless because of qtop.desktop).
  * Remove debian/patches/fixARM.patch: Integrated in new upstream release.
  * debian/control:
    - Bump Standards-Version to 3.9.8.
    - Update Vcs-Browser and Homepage fields to use an encrypted
      transport protocol.
  * debian/copyright:
    - Update copyright years.
  * debian/rules:
    - Add hardening flags.

 -- Hugo Lefeuvre <hle@debian.org>  Tue, 13 Sep 2016 11:06:26 +0200

qtop (2.3.1-1) unstable; urgency=low

  * Upload to unstable.
  * New upstream release.
  * debian/patches:
    - Refresh patches for new upstream version.
    - Make patches DEP3 compliant.
  * Add debian/menu and debian/add_desktop.patch, so qtop has a valid
    menu entry.
  * debian/qtop.1:
    - Refresh manpage for new upstream version.
  * debian/control:
    - Make description clearer (replace 'It' by 'qtop').
    - Include dpkg buildflags.
  * debian/control, debian/copyright:
    - Change the Maintainer's e-mail address.

 -- Hugo Lefeuvre <hle@debian.org>  Sun, 20 Sep 2015 12:39:26 +0200

qtop (2.2.5-1) experimental; urgency=low

  * New upstream version.
  * debian/control:
    - Bump Standards-Version to 3.9.6.
    - Add Vcs-Svn and Vcs-Browser fields.
    - Fix Lintian build-depends-on-metapackage error: Replace qt4-default
      build-dependency by libqt4-dev.
  * debian/watch:
    - Update URL as the old one resulted in "403 permission denied".
  * debian/copyright:
    - Update copyright years.
  * debian/patches/name.patch:
    - Refresh patches for new upstream version.
  * debian/qtop.1:
    - Update year and version.

 -- Hugo Lefeuvre <hugo6390@orange.fr>  Sat, 24 Jan 2015 11:40:21 +0100

qtop (2.2.4-1) unstable; urgency=low

  * Initial release (Closes: #731881)
  * debian/patches/name.patch:
    - Changes the name of the binary from Top to qtop.

 -- Hugo Lefeuvre <hugo6390@orange.fr>  Mon, 09 Jun 2014 10:37:00 +0200
