/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "ConfigurationDialog.h"

#include "Debug.h"
#include "GridLayout.h"
#include "IconEngine.h"
#include "IconNames.h"
#include "OptionBrowsedLineEditor.h"
#include "OptionCheckBox.h"
#include "OptionColorDisplay.h"
#include "OptionSlider.h"
#include "OptionSpinBox.h"

#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QPushButton>

//_________________________________________________________
ConfigurationDialog::ConfigurationDialog( QWidget* parent ):
    BaseConfigurationDialog( parent )
{

    Debug::Throw( "ConfigurationDialog::ConfigurationDialog.\n" );
    setWindowTitle( tr( "Configuration - Top" ) );

    // base configuration
    baseConfiguration( 0 );

    QWidget* page;
    QWidget *box;
    OptionColorDisplay *display;
    GridLayout* gridLayout;
    OptionCheckBox* checkbox;
    QLabel* label;

    // Job record
    page = &addPage( IconEngine::get( IconNames::PreferencesAppearance ), tr( "Appearance" ), tr( "Main window appearance" ) );

    // misc
    box = new QGroupBox( tr( "Options" ), page );
    page->layout()->addWidget( box );

    QVBoxLayout* layout = new QVBoxLayout;
    layout->setMargin(5);
    layout->setSpacing(5);
    box->setLayout( layout );

    checkbox = new OptionCheckBox( tr( "Use tree view to display process hierarchy" ), box, "TREE_VIEW" );
    addOptionWidget( checkbox );
    layout->addWidget( checkbox );

    checkbox = new OptionCheckBox( tr( "Show application icons when available" ), box, "SHOW_ICONS" );
    addOptionWidget( checkbox );
    layout->addWidget( checkbox );

    checkbox = new OptionCheckBox( tr( "Show process' full name when available" ), box, "SHOW_FULL_NAMES" );
    addOptionWidget( checkbox );
    layout->addWidget( checkbox );

    checkbox = new OptionCheckBox( tr( "Record all processes' cpu and memory usage" ), box, "RECORD_ALL_JOBS" );
    addOptionWidget( checkbox );
    layout->addWidget( checkbox );

    // refresh
    box = new QGroupBox( tr( "Unsorted" ), page );
    page->layout()->addWidget( box );

    QHBoxLayout* hLayout = new QHBoxLayout;
    hLayout->setSpacing(5);
    hLayout->setMargin(5);
    box->setLayout( hLayout );

    hLayout->addWidget( label = new QLabel( tr( "Refresh:" ), box ) );
    OptionSpinBox *refreshSpinbox = new OptionSpinBox( box, "REFRESH_RATE" );
    refreshSpinbox->setSuffix( tr( "s" ) );
    refreshSpinbox->setMinimum( 1 );
    refreshSpinbox->setMaximum( 60 );
    addOptionWidget( refreshSpinbox );
    label->setBuddy( refreshSpinbox );
    refreshSpinbox->setToolTip( tr( "Time interval (seconds) between two refresh" ) );
    hLayout->addWidget( refreshSpinbox );
    hLayout->addStretch( 1 );

    // Job record
    page = &addPage( IconEngine::get( IconNames::PreferencesJobTracking ), tr( "Process Statistics" ), tr( "Process statistics' settings and appearance" ) );

    // job record
    box = new QWidget( page );
    gridLayout = new GridLayout;
    gridLayout->setSpacing(5);
    gridLayout->setMargin(5);
    gridLayout->setMaxCount(2);
    gridLayout->setColumnAlignment( 0, Qt::AlignRight|Qt::AlignVCenter );

    box->setLayout( gridLayout );
    page->layout()->addWidget( box );

    // colors
    gridLayout->addWidget( label = new QLabel( tr( "Foreground:" ), box ) );
    display = new OptionColorDisplay( box, "FOREGROUND_COLOR" );
    addOptionWidget( display );
    label->setBuddy( display );
    gridLayout->addWidget( display );

    gridLayout->addWidget( label = new QLabel( tr( "Divisions:" ), box ) );
    display = new OptionColorDisplay( box, "DIV_COLOR" );
    addOptionWidget( display );
    label->setBuddy( display );
    gridLayout->addWidget( display );

    // divisions
    gridLayout->addWidget( label = new QLabel( tr( "Number of divisions along x:" ), box ) );
    OptionSpinBox* spinbox = new OptionSpinBox( box, "DIV_X" );
    spinbox->setMinimum( 0 );
    spinbox->setMaximum( 200 );
    addOptionWidget( spinbox );
    label->setBuddy( spinbox );
    spinbox->setToolTip( tr( "Number of vertical bars" ) );
    gridLayout->addWidget( spinbox );

    gridLayout->addWidget( label = new QLabel( tr( "Number of divisions along y:" ), box ) );
    spinbox = new OptionSpinBox( box, "DIV_Y" );
    spinbox->setMinimum( 0 );
    spinbox->setMaximum( 200 );
    addOptionWidget( spinbox );
    label->setBuddy( spinbox );
    spinbox->setToolTip( tr( "Number of horizontal bars" ) );
    gridLayout->addWidget( spinbox );

    // samples
    gridLayout->addWidget( label = new QLabel( tr( "Record length:" ), box ) );
    gridLayout->addWidget( spinbox = new OptionSpinBox( box, "RECORD_LENGTH" ) );
    spinbox->setMinimum( 1 );
    spinbox->setMaximum( 1024 );
    spinbox->setToolTip( tr( "Maximum number of data stored" ) );
    addOptionWidget( spinbox );
    label->setBuddy( spinbox );

    gridLayout->addWidget( label = new QLabel( "Samples: ", box ) );
    gridLayout->addWidget( spinbox = new OptionSpinBox( box, "SAMPLES" ) );
    spinbox->setMinimum( 1 );
    spinbox->setToolTip( tr( "Number of samples averaged for each measurement" ) );
    addOptionWidget( spinbox );
    label->setBuddy( spinbox );

    // load initial configuration
    read();

}
