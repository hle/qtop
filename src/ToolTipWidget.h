#ifndef ToolTipWidget_h
#define ToolTipWidget_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "BaseToolTipWidget.h"
#include "Job.h"

#include <QIcon>
#include <QFrame>
#include <QLabel>

class GridLayoutItem;

class ToolTipWidget: public BaseToolTipWidget
{

    Q_OBJECT

    public:

    //* constructor
    explicit ToolTipWidget( QWidget* );

    //* set data
    void setJob( const Job&, const QIcon& = QIcon() );

    //* mask
    void setPixmapSize( int value )
    {
        if( pixmapSize_ == value ) return;
        pixmapSize_ = value;
        reload();
    }

    //* reload
    void reload()
    { setJob( job_, icon_ ); }

    private Q_SLOTS:

    //* update configuration
    void _updateConfiguration();

    private:

    //* pixmap size
    int pixmapSize_ = 96;

    //* local icon copy
    QIcon icon_;

    //* local fileInfo copy
    Job job_;

    //* icon label
    QLabel* iconLabel_ = nullptr;

    //* file name label
    QLabel* commandLabel_ = nullptr;

    //* separator
    QFrame* separator_ = nullptr;

    //*@name items
    //@{
    GridLayoutItem* userItem_ = nullptr;
    GridLayoutItem* pidItem_ = nullptr;
    GridLayoutItem* ppidItem_ = nullptr;
    GridLayoutItem* timeItem_ = nullptr;
    //@}

};

#endif
