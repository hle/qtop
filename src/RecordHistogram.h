#ifndef RecordHistogram_h
#define RecordHistogram_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "Counter.h"

#include <QVector>

#include <QColor>
#include <QLabel>
#include <QScrollArea>

class HistogramWidget;

//* draw a set of vertical bars and x/y axis
class RecordHistogram: public QWidget, private Base::Counter<RecordHistogram>
{

    Q_OBJECT

    public:

    //* constructor
    explicit RecordHistogram( QWidget* );

    //* customized scrollview
    class ScrollArea: public QScrollArea, private Base::Counter<ScrollArea>
    {

        public:

        //* constructor
        explicit ScrollArea( QWidget* );

        //* retrieve histogram
        HistogramWidget& histogram()
        { return *histogram_; }

        protected:

        //* resize event method
        void resizeEvent( QResizeEvent* ) override;

        private:

        //* embedded histogram
        HistogramWidget* histogram_;

    };

    ScrollArea& scrollArea() const
    { return *scrollView_; }

    //* retrieve histogram
    HistogramWidget& histogram()
    { return scrollView_->histogram(); }

    //* adds a list of values
    void setValues( const QVector<double>& );

    private:

    //* scroll view
    ScrollArea *scrollView_;

};

#endif
