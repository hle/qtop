#ifndef JobThread_h
#define JobThread_h

/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "Counter.h"
#include "Job.h"

#include <QElapsedTimer>
#include <QHash>
#include <QMutex>
#include <QThread>

//* independent thread used to automatically save file
class JobThread: public QThread, private Base::Counter<JobThread>
{

    Q_OBJECT

    public:

    //* constructor
    explicit JobThread( QObject* = 0 );

    //* reset user map
    void resetUserMap();

    //* return set of jobs
    const Job::Set& jobs() const
    { return jobs_; }

    protected:

    //* Check files validity. Post a ValidFileEvent when finished
    void run() override;

    //* read total available memory
    void _readTotalMemory();

    //* read user map (might better move to a thread)
    void _readUserMap();

    private:

    //* mutex
    QMutex mutex_;

    //* timer
    QElapsedTimer timer_;

    //*@name user map
    //@{

    //* map user id to user name
    using UserMap = QHash<qint64, QString>;

    //* user map initialized
    bool userMapInitialized_ = false;

    //* user map
    UserMap userMap_;

    //@}

    //*@name total memory
    //@{

    //* total memory
    bool totalMemoryInitialized_ = false;

    //* total memory
    qint64 totalMemory_ = 0;

    //@}

    //* jobs
    Job::Set jobs_;

};
#endif
