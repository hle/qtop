/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "Debug.h"
#include "HistogramWidget.h"
#include "Options.h"
#include "RecordHistogram.h"

#include <QLayout>
#include <QPainter>
#include <QTextStream>
#include <QScrollBar>

#include <algorithm>

//________________________________________________________
RecordHistogram::RecordHistogram( QWidget* parent ):
    QWidget( parent ),
    Counter( "RecordHistogram" ),
    scrollView_( new ScrollArea( this ) )
{

    // create vbox layout
    QVBoxLayout* layout=new QVBoxLayout;
    layout->setMargin(0);
    layout->setSpacing(2);
    setLayout( layout );

    layout->addWidget( scrollView_ );
    layout->activate();

    histogram().addDataSet( HistogramWidget::DataSet( tr( "generic" ), "FOREGROUND_COLOR" ) );

}

//________________________________________________________
RecordHistogram::ScrollArea::ScrollArea(  QWidget* parent ):
    QScrollArea( parent ),
    Counter( "RecordHistogram::ScrollArea" ),
    histogram_( new HistogramWidget( this ) )
{

    setBackgroundRole( QPalette::Base );

    setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    setWidget( histogram_ );
    connect( horizontalScrollBar(), SIGNAL(valueChanged(int)), histogram_, SLOT(update()) );

}

//________________________________________________________
void RecordHistogram::setValues( const QVector<double>& values )
{
    Debug::Throw( "RecordHistogram::updateValue.\n" );

    // check array length
    if( values.empty() ) return;
    histogram().dataSet(0).setValues( values );

    scrollArea().viewport()->update();
    histogram().update();

}

//________________________________________________________
void RecordHistogram::ScrollArea::resizeEvent( QResizeEvent *event )
{
    Debug::Throw( "RecordHistogram::ScrollArea::resizeEvent.\n" );

    // resize the scrollview
    QScrollArea::resizeEvent( event );

    // resize the histogram subclass so that it gets the correct height
    histogram().setVisibleHeight( viewport()->height() );
    histogram().resize( histogram().width(), event->size().height() );

    return;
}
