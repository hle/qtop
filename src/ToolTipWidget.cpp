/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "ToolTipWidget.h"
#include "Debug.h"
#include "GridLayout.h"
#include "QtUtil.h"
#include "Singleton.h"
#include "TimeStamp.h"
#include "GridLayoutItem.h"
#include "XmlOptions.h"

#include <QLayout>

//_______________________________________________________
ToolTipWidget::ToolTipWidget( QWidget* parent ):
    BaseToolTipWidget( parent )
{

    Debug::Throw( "ToolTipWidget::ToolTipWidget.\n" );

    // layout
    QHBoxLayout* hLayout = new QHBoxLayout;
    hLayout->setMargin( 10 );
    hLayout->setSpacing( 10 );
    setLayout( hLayout );

    hLayout->addWidget( iconLabel_ = new QLabel( this ) );
    iconLabel_->setAlignment( Qt::AlignHCenter|Qt::AlignTop );

    QVBoxLayout* vLayout = new QVBoxLayout;
    vLayout->setMargin( 0 );
    vLayout->setSpacing( 5 );
    hLayout->addLayout( vLayout );

    // file
    vLayout->addWidget( commandLabel_ = new QLabel( this ) );
    commandLabel_->setAlignment( Qt::AlignCenter );
    commandLabel_->setFont( QtUtil::titleFont( commandLabel_->font() ) );

    // separator
    vLayout->addWidget( separator_ = new QFrame( this ) );
    separator_->setFrameStyle( QFrame::HLine );

    // grid layout
    GridLayout* gridLayout = new GridLayout;
    gridLayout->setMaxCount( 2 );
    gridLayout->setColumnAlignment( 0, Qt::AlignVCenter|Qt::AlignRight );
    gridLayout->setColumnAlignment( 1, Qt::AlignVCenter|Qt::AlignLeft );
    gridLayout->setMargin( 0 );
    gridLayout->setSpacing( 5 );
    vLayout->addLayout( gridLayout );

    // items
    ( userItem_ = new GridLayoutItem( this, gridLayout ) )->setKey( tr( "User:" ) );
    ( pidItem_ = new GridLayoutItem( this, gridLayout ) )->setKey( tr( "Process Id:" ) );
    ( ppidItem_ = new GridLayoutItem( this, gridLayout ) )->setKey( tr( "Parent's Id:" ) );
    ( timeItem_ = new GridLayoutItem( this, gridLayout ) )->setKey( tr( "CPU Time:" ) );

    // add stretch
    vLayout->addStretch( 1 );

    // configuration
    connect( Base::Singleton::get().application(), SIGNAL(configurationChanged()), SLOT(_updateConfiguration()) );
    _updateConfiguration();

}

//_______________________________________________________
void ToolTipWidget::setJob( const Job& job, const QIcon& icon )
{
    Debug::Throw( "ToolTipWidget::setFileInfo.\n" );

    // local storage
    icon_ = icon;
    if( icon_.isNull() ) icon_ = job.icon();
    job_ = job;

    // update icon
    if( !icon_.isNull() )
    {

        iconLabel_->setPixmap( icon_.pixmap( QSize( pixmapSize_, pixmapSize_ ) ) );
        iconLabel_->show();

    } else iconLabel_->hide();

    // job command and separator
    const QString name = job.longName().isEmpty() ? job.name():job.longName();
    if( !name.isEmpty() )
    {

        commandLabel_->show();
        commandLabel_->setText( name );
        separator_->show();

    } else {

        commandLabel_->hide();
        separator_->hide();

    }

    // user
    if( !job.user().isEmpty() )
    {

        userItem_->setText( job.user() );
        userItem_->show();

    } else userItem_->hide();

    // pid
    pidItem_->setText( QString::number( abs( job.id() ) ) );
    pidItem_->show();

    // parent id
    if( job.parentId() > 0 )
    {

        ppidItem_->setText( QString::number( job.parentId() ) );
        ppidItem_->show();

    } else ppidItem_->hide();

    // up time
    if( job.cpuTime() > 0 )
    {

        timeItem_->setText( job.cpuTimeString() );
        timeItem_->show();

    } else timeItem_->hide();

    adjustSize();

}

//_____________________________________________
void ToolTipWidget::_updateConfiguration()
{
    Debug::Throw( "ToolTipWidget::_updateConfiguration.\n" );
    if( XmlOptions::get().contains( "TOOLTIPS_PIXMAP_SIZE" ) ) setPixmapSize( XmlOptions::get().get<int>( "TOOLTIPS_PIXMAP_SIZE" ) );
}
