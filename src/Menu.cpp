/******************************************************************************
*
* Copyright (C) 2002 Hugo PEREIRA <mailto: hugo.pereira@free.fr>
*
* This is free software; you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation; either version 2 of the License, or (at your option) any later
* version.
*
* This software is distributed in the hope that it will be useful, but WITHOUT
* Any WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "Menu.h"

#include "Debug.h"
#include "DebugMenu.h"
#include "IconNames.h"
#include "IconEngine.h"
#include "Application.h"
#include "MainWindow.h"
#include "Singleton.h"
#include "JobInformationDialog.h"
#include "XmlOptions.h"

#include <QMenu>

//__________________________________________________
Menu::Menu( QWidget* parent ):
    QMenuBar( parent ),
    Counter( "Menu" )
{

    Debug::Throw( "Menu::Menu\n" );

    // generic menu
    QMenu *menu;

    // application and top widget
    Application& application( *static_cast<Application*>(Base::Singleton::get().application()) );
    MainWindow& mainWindow( *static_cast<MainWindow*>(topLevelWidget()) );

    // file menu
    menu = addMenu( tr( "File" ) );
    menu->addAction( &mainWindow.addFrameAction() );
    menu->addAction( &mainWindow.removeFrameAction() );
    menu->addAction( &mainWindow.updateAction() );
    menu->addSeparator();
    menu->addAction( &application.closeAction() );

    // windows menu
    windowsMenu_ = addMenu( tr( "Windows" ) );
    connect( windowsMenu_, SIGNAL(aboutToShow()), SLOT(_updateWindowsMenu()) );

    // Settings menu
    menu = addMenu( tr( "Settings" ) );
    menu->addAction( &mainWindow.toggleRecordAction() );
    menu->addAction( &mainWindow.treeViewAction() );
    menu->addAction( &mainWindow.toggleSummaryAction() );
    menu->addSeparator();
    menu->addAction( &application.configurationAction() );

    // create help menu
    menu = addMenu( tr( "Help" ) );
    menu->addAction( &application.aboutAction() );
    menu->addAction( &application.aboutQtAction() );

    // debug menu
    menu->addSeparator();
    DebugMenu *debugMenu( new DebugMenu( menu ) );
    menu->addAction( debugMenu->menuAction() );
    debugMenu->menuAction()->setVisible( false );

}

//____________________________________________________________________
void Menu::_updateWindowsMenu()
{

    Debug::Throw( "Menu::_updateWindowsMenu.\n" );

    // clear menu
    windowsMenu_->clear();

    MainWindow& mainWindow( *static_cast<MainWindow*>(topLevelWidget()) );
    Base::KeySet<JobInformationDialog> dialogs( &mainWindow );

    QAction* action(0);
    for( const auto& dialog:dialogs )
    {
        const QString buffer = QString( tr( "Job %1" ) ).arg( dialog->record().id() );
        windowsMenu_->addAction( action = new QAction( buffer, windowsMenu_ ) );
        connect( action, SIGNAL(triggered()), dialog, SLOT(uniconify()) );
    }

    if( !dialogs.empty() ) windowsMenu_->addSeparator();

    // close job dialogs
    windowsMenu_->addAction( action = new QAction( IconEngine::get( IconNames::DialogClose ), tr( "Close Process Statistics' Dialogs" ), windowsMenu_ ) );
    connect( action, SIGNAL(triggered()), &mainWindow, SLOT(closeJobInformationDialogs()) );
    action->setEnabled( !dialogs.empty() );

    return;

}
